<?php
/**
 * Created by PhpStorm.
 * User: valentinloiseau
 * Date: 2019-02-24
 * Time: 17:38
 */

set_time_limit(600);
require_once 'api-key.php';

$csv = ($_FILES['csv']) ?? null;
$delimiter = ($_POST['delimiteur']) ?? null;
$column = ($_POST['colonne']) ?? null;

if ($csv) {
    if (pathinfo($csv['name'], PATHINFO_EXTENSION) === 'csv') {
        $data = explode(PHP_EOL, file_get_contents($csv['tmp_name']));
        $response = array();

        foreach ($data as $line) {
            $address = explode($delimiter, $line)[$column - 1];

            if (!empty($address)) {
                $query = http_build_query(array(
                    'key' => $googleApiKey,
                    'address' => $address
                ));

                $ch = curl_init("https://maps.googleapis.com/maps/api/geocode/json?$query");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

                $result = json_decode(curl_exec($ch))->results[0];
                curl_close($ch);

                $response[] = array(
                    $address,
                    $result->formatted_address,
                    $result->geometry->location->lat,
                    $result->geometry->location->lng
                );
            }
        }

        @unlink($csv['tmp_name']);

        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=result.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        echo 'Adresse recherchée'.$delimiter.'Adresse trouvée'.$delimiter.'Latitude'.$delimiter."Longitude\n";
        foreach ($response as $value) {
            foreach ($value as $key => $item) {
                echo $item;
                echo ($key === count($value)-1)
                    ? "\n"
                    : $delimiter;
            }
        }

        die();
    }
}

?>

<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Geocoder</title>
</head>
<body>
<h1>Geocoder</h1>

<form method="post" enctype="multipart/form-data">
    <label for="csv">Fichier csv :</label>
    <input type="file" name="csv" id="csv" required>

    <label for="delimiteur">Delimiteur :</label>
    <input type="text" name="delimiteur" id="delimiteur" required>

    <label for="colonne">Colonne :</label>
    <input type="number" name="colonne" id="colonne" value="1" required>

    <input type="submit" value="Envoyer">
</form>
</body>
</html>
